<?php

/**
 * http://sito/read.php
 * visualizza la scritta:
 * {"messaggio":"ciao"}
 */

if(!file_exists('chat.txt')) {
    $messaggio = "1";
    file_put_contents('chat.txt',"1");
}
else {

    if (file_get_contents('chat.txt') == "1") {
        $messaggio = "2";
        file_put_contents('chat.txt', "2");
    } else {
        $messaggio = file_get_contents('chat.txt');
    }
}

echo json_encode(array('messaggio'=>$messaggio));

<?php



session_start();

$matrice;

if($_SESSION["turno"]==NULL)  {
    $_SESSION["turno"] = 0;
}
if($_SESSION["giocatore"] == NULL) {
    $_SESSION["giocatore"] = 1;
}

if(file_get_contents('chat.txt') == '2') {
    creaMatrice();
    file_put_contents('chat.txt',json_encode($matrice));
}

$messaggio=$_POST['messaggio'];
$giocatore=$_POST['giocatore'];
if($messaggio==8) {
    $messaggio = 0;
}
$matrice = json_decode(file_get_contents("chat.txt"));
turno($messaggio,$giocatore);



function turno($messaggio, $giocatore) {
    GLOBAL $matrice;
    file_put_contents('debug.txt',"inizio turno");

    if($giocatore == $_SESSION["giocatore"]) //controllo che sia il turno giusto
    {
        $mossa = $messaggio;
        //$mossa--;
        if(controlloMossa($mossa)) {

            $simbolo = "x";
            if($_SESSION["giocatore"] == 1) {
                $simbolo = "x";
            }
            else $simbolo= "o";
            inserisciPedina($mossa,$simbolo);
            file_put_contents('debug.txt',"ho mosso");

            if(controlloVittoria($mossa))  {
                file_put_contents('chat.txt',"v".$_SESSION["giocatore"]);
                sleep(5);
                unlink('chat.txt');
                return true;
            }
            if(isPiena()) {
               file_put_contents('chat.txt',"patta");
                sleep(5);
                unlink('chat.txt');
                return true;
            }

            file_put_contents('chat.txt',json_encode($matrice)) ; //inserisco la matrice nel file sotto forma di json
            if($_SESSION["giocatore"] == 1) {
                $_SESSION["giocatore"] = 2;
            }
            else $_SESSION["giocatore"] = 1;
            $_SESSION["turno"]++;
        }
        file_put_contents('debug.txt',"mossa non valida");
    }
    file_put_contents('debug.txt',"non è il tuo turno");

}

function creaMatrice() {
    global $matrice;
     $matrice = array(
        array(' ',' ',' ',' ',' ',' ',' '),
        array(' ',' ',' ',' ',' ',' ',' '),
        array(' ',' ',' ',' ',' ',' ',' '),
        array(' ',' ',' ',' ',' ',' ',' '),
        array(' ',' ',' ',' ',' ',' ',' '),
        array(' ',' ',' ',' ',' ',' ',' ')
    );
}

function controlloMossa($colonna) {
    //ricordarsi indici da 0 o da 1
    global $matrice;
    if($matrice[0][$colonna] == ' ')
    {
        return true;
    }
    return false;
}

function inserisciPedina($colonna, $simbolo) {
    global $matrice;
    $riga = 0;
    while($matrice[$riga][$colonna] == ' ') {
        $riga++;
    }
    $matrice[$riga -1][$colonna] = $simbolo;

}

function controlloVittoria($colonna) {
    global $matrice;
    //identifico la riga
    $riga = 0;
    while($matrice[$riga][$colonna]==' ') {
        $riga++;
    }

    //controllo orizzontale
    $contatore = 1;
    $temp = $colonna - 1;
    while($temp >= 0 && $matrice[$riga][$temp] ==  $matrice[$riga][$colonna]) {
        $temp--;
        $contatore++;
    }
    $temp = $colonna + 1;
    while($temp <= 6 && $matrice[$riga][$temp] ==  $matrice[$riga][$colonna]) {
        $temp ++;
        $contatore++;
    }

    if($contatore >= 4)
        return true;

    //controllo verticale
    $contatore = 1;
    $temp = $riga + 1;
    while($temp <= 5 && $matrice[$temp][$colonna] ==  $matrice[$riga][$colonna]) {
        $temp++;
        $contatore++;
    }

    if($contatore >= 4)
        return true;

    //controllo diagonale basso sx alto dx
    $contatore = 1;
    $temp = $riga +1;
    $temp2 = $colonna  -1;
    while($temp >= 5 && $temp2>=0 && $matrice[$temp][$temp2] ==  $matrice[$riga][$colonna]) {
        $temp++;
        $temp2--;
        $contatore++;
    }
    $temp = $riga -1;
    $temp2 = $colonna + 1;
    while($temp >= 0 && $temp2<=6 && $matrice[$temp][$temp2] ==  $matrice[$riga][$colonna]) {
        $temp--;
        $temp2++;
        $contatore++;
    }

    if($contatore >= 4)
        return true;

    //controllo diagonale alto sx basso dx
    $contatore = 1;
    $temp = $riga + 1;
    $temp2 = $colonna +1;
    while($temp <= 5 && $temp2<=6 && $matrice[$temp][$temp2] ==  $matrice[$riga][$colonna]) {
        $temp++;
        $temp2++;
        $contatore++;
    }
    $temp = $riga -1;
    $temp2 = $colonna - 1;
    while($temp >= 0 && $temp2>=0 && $matrice[$temp][$temp2] ==  $matrice[$riga][$colonna]) {
        $temp--;
        $temp2--;
        $contatore++;
    }

    if($contatore >= 4)
        return true;

    return false;
}

function isPiena() {
    GLOBAL $matrice;
    for($counter = 0; $counter < 6; $counter++) {
        if($matrice[0][$counter] == ' ') {
            return false;
        }
    }
    return true;
}

